/* Copyright 2020, E. Sergio Burgos.
 * All rights reserved.
 *
 * This file is part bShell library for microcontrollers.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef CMD_INC_CMDBSHELL_H_
#define CMD_INC_CMDBSHELL_H_

#include "cmdConfig.h"

#include <stdint.h>

#define stKey 0
#define stBracket 1
#define stEsc     2
#define stEnd     3

typedef void (*sendFcnPtr)(char);
typedef char (*reciveFcnPtr)(void);

typedef struct
{
	char     buff[LINELEN];
	uint32_t buffInx;
	uint32_t curPos;
	uint8_t  insMode;
	uint8_t  rcvState;
	uint32_t ovrFlw;
	uint8_t  argc;
	char    *argv[MAXARGCNT];
	sendFcnPtr sndPort;
	reciveFcnPtr rcvPort;
#ifdef USEHISTORY
	char    hist[HISTORYLEN][LINELEN];
	uint8_t hRead;
	uint8_t hWrite;
#endif
}cmdInstance;

typedef uint8_t (*commandFcn)(cmdInstance *);

typedef struct{
	char * commandName;
	commandFcn act;
	char * help;
}commandEntry;

void sendStr(cmdInstance *, char *);
int xcmp(char *, char *);
void clearCmdLine(char *txt);

typedef uint8_t (*ptrStateFcn)(cmdInstance *, uint8_t);

void cmdInit(cmdInstance * inst, sendFcnPtr sendFcn, reciveFcnPtr rcvFcn);
void cmdProcessNewChar(cmdInstance * inst, uint8_t newByte);
void cmdExecute(cmdInstance * inst);

uint8_t stFcnChr(cmdInstance *, uint8_t);    // 0
uint8_t stFcnBracket(cmdInstance *, uint8_t);// 1
uint8_t stFcnEsc(cmdInstance *, uint8_t);    // 2
uint8_t stFcnEnd(cmdInstance *, uint8_t);    // 3

uint8_t scClearScr(cmdInstance * inst);
uint8_t scHelp(cmdInstance * inst);
uint8_t scTestArgs(cmdInstance * inst);
uint8_t scTestIO(cmdInstance * inst);


#endif /* CMD_INC_CMDBUFF_H_ */
