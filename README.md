# bShell

bShell es una librería para la implementación de terminales de comandos en sistemas embebidos. Utiliza compatilibidad básica con secuencias de control VT-100 a fin de mejorar la experiencia de usuario e incluye la posibilidad de utilizar un histórico de comandos. 

La principal ventaja de esta librería frente a otras alternativas, es que, a través del uso del atributo `section` disponible en GCC almacena los comandos como estructuras inicializadas en memoria de programa. Posibilitando de este modo que sean identificados al finalizar el ingreso de una línea de comandos.

Por la forma de la implementación de los comandos, cada uno se implementa como una función en lenguaje C. Más detalles sobre su uso y configuración encontrará en las carpetas [bluePill](ejemplos/bluePill/README.md) y [lpc4337](ejemplos/lpc4337/README.md) que contienen ejemplos para cada caso. Si lo desea, [aquí](testBasicShell.mp4) podrá ver un video donde se muestra el uso bShell en cada plataforma.  


