/* Copyright 2020, E. Sergio Burgos.
 * All rights reserved.
 *
 * This file is part bShell library for microcontrollers.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef INC_CMDESCDEF_H_
#define INC_CMDESCDEF_H_

#define ESCKEY     '\e'
#define BACKSPACE (0x08)

#define UPKEY        'A'
#define DWNKEY       'B'
#define LEFTKEY      'D'
#define RIGHTKEY     'C'
#define DELKEY       '3'
#define HOMEKEY      'H'
#define ENDKEY       'F'
#define INSKEY       '2'
#define PGUPKEY      '5'
#define PGDWNKEY     '6'
#define F1KEY        'P'
#define F2KEY        'Q'
#define F3KEY        'R'
#define F4KEY        'S'

#define escClrScr   "\e[2J"
#define escSetHome  "\e[H"
#define clrLine     "\e[2K"
#define crsrLeft    "\e[1D"
#define crsrRight   "\e[1C"
#define crsrSave    "\e[s"
#define crsrRestore "\e[u"


#endif /* INC_CMDESCDEF_H_ */
