/* Copyright 2020, E. Sergio Burgos.
 * All rights reserved.
 *
 * This file is part bShell library for microcontrollers.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef INC_CMDCOLORS_H_
#define INC_CMDCOLORS_H_

#define fDefault      "\e[39m"
#define fBlack        "\e[30m"
#define fRed          "\e[31m"
#define fGreen        "\e[32m"
#define fYellow       "\e[33m"
#define fBlue         "\e[34m"
#define fMagenta      "\e[35m"
#define fCyan         "\e[36m"
#define fLightGray    "\e[37m"
#define fDarkGray     "\e[90m"
#define fLightRed     "\e[91m"
#define fLightGreen   "\e[92m"
#define fLightYellow  "\e[93m"
#define fLightBlue    "\e[94m"
#define fLightMagenta "\e[95m"
#define fLightCyan    "\e[96m"
#define fWhite        "\e[97m"

#define bDefault      "\e[49m"
#define bBlack        "\e[40m"
#define bRed          "\e[41m"
#define bGreen        "\e[42m"
#define bYellow       "\e[43m"
#define bBlue         "\e[44m"
#define bMagenta      "\e[45m"
#define bCyan         "\e[46m"
#define bLightGray    "\e[47m"
#define bDarkGray     "\e[100m"
#define bLightRed     "\e[101m"
#define bLightGreen   "\e[102m"
#define bLightYellow  "\e[103m"
#define bLightBlue    "\e[104m"
#define bLightMagenta "\e[105m"
#define bLightCyan    "\e[106m"
#define bWhite        "\e[107m"

#endif /* INC_CMDCOLORS_H_ */
