/* Copyright 2020, E. Sergio Burgos.
 * All rights reserved.
 *
 * This file is part bShell library for microcontrollers.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "../inc/cmdBShell.h"
#include "cmdColors.h"
#include "cmdEscDef.h"

/* Estos símbolos DEBEN estar definidos en el linker script
 *
 * Observe en la carpeta ld, el arechivo areas y encontrará lo siguiente:
 *
 * ....
 *     .text : ALIGN(4)
 *   {
 *        *(.text*)
 *       *(.rodata .rodata.* .constdata .constdata.*)
 *       . = ALIGN(4);
 *       PROVIDE(etext = .);
 *
 *		commandsBegin = ABSOLUTE(.);
 *		KEEP(*(.cmdCommands))
 *		commandsEnd = ABSOLUTE(.);
 *
 *   } > flash
 *
 * ....
 * Esta definición debe realizarce a fin de poder alojar los comandos que
 * desee incluir en una sección específica de memoria.
 * */

extern uint32_t commandsBegin;
extern uint32_t commandsEnd;

#ifdef CMDSHOWLOGO
const char logo[] =
		" ___                              ___    _             _    _ \r\n" \
		"(  _`\\               _           (  _`\\ ( )           (_ ) (_ )\r\n" \
		"| (_) )   _ _   ___ (_)   ___    | (_(_)| |__     __   | |  | |\r\n" \
		"|  _ <' /'_` )/',__)| | /'___)   `\\__ \\ |  _ `\\ /'__`\\ | |  | |\r\n" \
		"| (_) )( (_| |\\__, \\| |( (___    ( )_) || | | |(  ___/ | |  | |\r\n" \
		"(____/'`\\__,_)(____/(_)`\\____)   `\\____)(_) (_)`\\____)(___)(___)\r\n" \
		"\r\n" \
		"\r\n" \
		"\r\n" \
		"( )    (  _`\\ ( )           (_ ) (_ )               /' )   /' _`\\ \r\n" \
		"| |_   | (_(_)| |__     __   | |  | |     _   _    (_, |   | ( ) |\r\n" \
		"| '_`\\ `\\__ \\ |  _ `\\ /'__`\\ | |  | |    ( ) ( )     | |   | | | |\r\n" \
		"| |_) )( )_) || | | |(  ___/ | |  | |    | \\_/ |     | | _ | (_) |\r\n" \
		"(_,__/'`\\____)(_) (_)`\\____)(___)(___)   `\\___/'     (_)(_)`\\___/'\r\n\r\n";
#endif

void sendStr(cmdInstance * inst, char * ch){
	while(*ch){
		inst->sndPort(*ch);
		ch++;
	}
}

int xcmp(char *t1, char *t2)
{
	int r = *t1 - *t2;
	while((*t1) && (*t2) && (!r))
	{
		r = *t1 - *t2;
		t1++;
		t2++;
	}
	return r + *t1 + *t2;
}

void clearCmdLine(char *txt)
{
    int k;
    int j;
    while(txt[0] == ' ')
    {
        j = 0;
        while(txt[j])
        {
            txt[j] = txt[j+1];
            j++;
        }
    }
    k = 0;
    while(txt[k])
    {
        if(txt[k] == ' ')
        {
            while(txt[k + 1] == ' ')
            {
                j = k;
                while(txt[j])
                {
                    txt[j] = txt[j+1];
                    j++;
                }
            }
        }
        k++;
    }
    if(k)
    {
       if(txt[k - 1] == ' ')
           txt[k - 1] = '\0';
    }
}

uint8_t stFcnChr(cmdInstance *inst, uint8_t rcv)
{
	uint8_t ret = 0;
	uint8_t k;
	if(rcv == '\r')
	{
		inst->buff[inst->buffInx] = '\0';
#ifdef USEHISTORY
		k = 0;
		inst->hRead = inst->hWrite;
		while(inst->buff[k])
		{
			inst->hist[inst->hWrite][k] = inst->buff[k];
			k++;
		}
		inst->hist[inst->hWrite][k] = '\0';
		inst->hWrite++;
		inst->hWrite = inst->hWrite % HISTORYLEN;
#endif
		inst->sndPort('\r');
		inst->sndPort('\n');
		clearCmdLine(inst->buff);
		inst->argv[0] = inst->buff;

		inst->argc = 1;
		if(inst->buff[0])
		{
			k = 1;
			while(inst->buff[k] && (inst->argc < MAXARGCNT))
			{
				if(inst->buff[k] == ' ')
				{
					inst->argv[inst->argc] = &inst->buff[k + 1];
					inst->buff[k] = '\0';
					inst->argc++;
				}
				k++;
			}
		}
		inst->buffInx = 0;
		inst->curPos = 0;

		ret = 1;
	}
	else if(rcv == BACKSPACE)
	{
		if(inst->curPos > 0)
		{
			inst->curPos--;
			inst->buffInx--;
			for(k = inst->curPos; k < inst->buffInx; k++)
				inst->buff[k] = inst->buff[k+1];
			inst->buff[inst->buffInx] = '\0';

			sendStr(inst, crsrSave);
			sendStr(inst, clrLine);
			sendStr(inst,"\r");
			sendStr(inst, PROMPT);
			sendStr(inst, inst->buff);
			sendStr(inst, crsrRestore);
			sendStr(inst, crsrLeft);
		}
	}
	else if(rcv == ESCKEY)
		inst->rcvState = stBracket;
	else
	{
		if(inst->curPos < inst->buffInx)
		{
			if((inst->insMode) && (inst->buffInx < LINELEN))
			{
				for(k = inst->buffInx; k > inst->curPos; k--)
					inst->buff[k] = inst->buff[k-1];
				inst->buff[inst->curPos] = rcv;
				inst->curPos++;
				inst->buffInx++;
				inst->buff[inst->buffInx] = '\0';
				inst->sndPort(rcv);
				sendStr(inst, crsrSave);
				sendStr(inst, clrLine);
				sendStr(inst, "\r");
				sendStr(inst, PROMPT);
				sendStr(inst, inst->buff);
				sendStr(inst, crsrRestore);
			}
			else
			{
				inst->buff[inst->curPos] = rcv;
				inst->curPos++;
				inst->sndPort(rcv);
			}
		}
		else if(inst->buffInx < LINELEN)
		{
			inst->buff[inst->buffInx] = rcv;
			inst->buffInx++;
			inst->curPos++;
			inst->sndPort(rcv);
		}
	}
	return ret;
}

uint8_t stFcnBracket(cmdInstance *inst, uint8_t rcv)
{
	uint8_t ret = 0;
	if((rcv == '[') ||(rcv == 'O'))
		inst->rcvState = stEsc;
	else
		inst->rcvState = stKey;
	return ret;
}

uint8_t stFcnEsc(cmdInstance *inst, uint8_t rcv)
{
	uint8_t ret = 0;
	uint8_t k;
	inst->rcvState = stKey;
	switch(rcv)
	{
	case UPKEY:
#ifdef USEHISTORY
		k = 0;
		inst->hRead ++;
		inst->hRead = inst->hRead % HISTORYLEN;

		while(inst->hist[inst->hRead][k]){
			inst->buff[k] = inst->hist[inst->hRead][k];
			k++;
		}
		inst->buff[k] = '\0';
		inst->curPos = k;
		inst->buffInx = k;

		sendStr(inst, clrLine);
		sendStr(inst, "\r");
		sendStr(inst, PROMPT);
		sendStr(inst, inst->buff);
#endif
		break;

	case DWNKEY:
#ifdef USEHISTORY
		if(inst->hRead == 0)
			inst->hRead = HISTORYLEN - 1;
		else
			inst->hRead --;
		k = 0;
		while(inst->hist[inst->hRead][k]){
			inst->buff[k] = inst->hist[inst->hRead][k];
			k++;
		}
		inst->buff[k] = '\0';
		inst->curPos = k;
		inst->buffInx = k;
		sendStr(inst, clrLine);
		sendStr(inst, "\r");
		sendStr(inst, PROMPT);
		sendStr(inst, inst->buff);
#endif
		break;

	case RIGHTKEY:
		if (inst->curPos < inst->buffInx)
		{
			inst->curPos++;
			sendStr(inst, crsrRight);
		}
		break;

	case LEFTKEY:
		if (inst->curPos > 0)
		{
			inst->curPos--;
			sendStr(inst, crsrLeft);
		}
		break;
	case DELKEY:
		inst->rcvState = stEnd;
		if(inst->buffInx - inst->curPos > 0)
		{
			for(k = inst->curPos; k < inst->buffInx - 1; k++)
				inst->buff[k] = inst->buff[k+1];
			inst->buffInx--;
			inst->buff[inst->buffInx] = '\0';
			sendStr(inst, crsrSave);
			sendStr(inst, clrLine);
			sendStr(inst, "\r");
			sendStr(inst, PROMPT);
			sendStr(inst, inst->buff);
			sendStr(inst, crsrRestore);
		}
		break;

	case HOMEKEY:
		inst->curPos = 0;
		sendStr(inst, "\r");
		for(k = 0; k < PROMPTLEN; k++)
			sendStr(inst, crsrRight);
		break;

	case ENDKEY:
		for(k = 0; k <inst->buffInx - inst->curPos; k++)
			sendStr(inst, crsrRight);
		inst->curPos = inst->buffInx;
		break;

	case PGDWNKEY:
		inst->rcvState = stEnd;
		break;

	case PGUPKEY:
		inst->rcvState = stEnd;
		break;

	case INSKEY:
		inst->insMode = !inst->insMode;
		inst->rcvState = stEnd;
		break;

	case F1KEY:
		break;

	case F2KEY:
		break;

	case F3KEY:
		break;

	case F4KEY:
		break;

	}
	return ret;
}

uint8_t stFcnEnd(cmdInstance *inst, uint8_t rcv)
{
	inst->rcvState = stKey;
	return 0;
}

void cmdInit(cmdInstance * inst, sendFcnPtr sendFcn, reciveFcnPtr rcvFcn)
{
	inst->buffInx = 0;
	inst->curPos = 0;
	inst->rcvState = 0;
	inst->sndPort = sendFcn;
	inst->rcvPort = rcvFcn;
	inst->argc = 0;
	inst->insMode = 0;
#ifdef USEHISTORY
	int k;
	inst->hRead = 0;
	inst->hWrite = 0;
	for(k=0; k < HISTORYLEN; k++)
		inst->hist[k][0] = '\0';
#endif
	sendStr(inst, BKGTERM);
	sendStr(inst, FRGTERM);
	sendStr(inst, escClrScr);
	sendStr(inst, escSetHome);
#ifdef CMDSHOWLOGO
	sendStr(inst, (char *)logo);
#endif
	sendStr(inst, PROMPT);
}

void cmdProcessNewChar(cmdInstance * inst, uint8_t newByte)
{
	static ptrStateFcn fcnStates[4] = {stFcnChr, stFcnBracket, stFcnEsc, stFcnEnd};

	if(fcnStates[inst->rcvState](inst, newByte))
		cmdExecute(inst);
}

void cmdExecute(cmdInstance * inst)
{
	commandEntry * entry = (commandEntry *)&commandsBegin;
	uint8_t cmdFound = 0;
	while((entry != (commandEntry *)&commandsEnd) && (!cmdFound))
	{
		cmdFound = !xcmp(entry->commandName, inst->argv[0]);
		if(cmdFound)
			entry->act(inst);
		entry++;
	}
	if((!cmdFound) && (inst->argv[0][0] != '\0' ))
	{
		sendStr(inst, CMDNF);
		sendStr(inst, inst->argv[0]);
		sendStr(inst, "\r\n");
	}
	sendStr(inst, PROMPT);
}

uint8_t scHelp(cmdInstance * inst)
{
	char *ch;
	commandEntry * entry = (commandEntry *)&commandsBegin;
	while(entry != (commandEntry *)&commandsEnd)
	{
		ch = entry->help;
		while(*ch)
		{
			inst->sndPort(*ch);
			ch++;
		}
		inst->sndPort('\n');
		inst->sndPort('\r')	;
		entry++;
	}
	return 0;
}

uint8_t scClearScr(cmdInstance * inst)
{
	sendStr(inst, BKGTERM);
	sendStr(inst, FRGTERM);
	sendStr(inst, escClrScr);
	sendStr(inst, escSetHome);
	return 0;
}

uint8_t scTestArgs(cmdInstance * inst)
{
	int k;
	sendStr(inst, "Argumentos utilizados:\r\n");
	for(k = 0; k < inst->argc; k++)
	{
		sendStr(inst, "\t- ");
		sendStr(inst, inst->argv[k]);
		sendStr(inst, "\r\n");
	}
	return 0;
}

uint8_t scTestIO(cmdInstance * inst)
{
	char txt[40];
	int k = 0;

	sendStr(inst, "Ingrese un texto:");
	do{
		txt[k] = inst->rcvPort();
		k++;
	}while((k < 39) && txt[k-1] != '\r');
	txt[k]='\0';
	sendStr(inst, "\n");
	sendStr(inst, "Ingresó:");
	sendStr(inst, txt);
	sendStr(inst, "\r\n");
	return 0;
}

__attribute__ ((used,section(".cmdCommands")))
const commandEntry systemCommands [] = {
		{
				"help", /* Command name */
				scHelp, /* Associated function */
				"help: muestra información sobre los comandos disponibles" /* Command help */
		},
		{
				"cls", /* Command name */
				scClearScr, /* Associated function */
				"cls: borra la pantalla" /* Command help */
		},
		{
				"args", /* Command name */
				scTestArgs, /* Associated function */
				"args: muestra los argumentos recibidos por la función" /* Command help */
		},
		{
				"testIO", /* Command name */
				scTestIO, /* Associated function */
				"testIO: ejemplo de implementación de entrada salida" /* Command help */
		},
};
