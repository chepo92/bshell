/* Copyright 2020, E. Sergio Burgos.
 * All rights reserved.
 *
 * This file is part bShell library for microcontrollers.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "chip.h"
#include <lpc_types.h>
#include "../inc/cmdBShell.h"
#include "sysUtils.h"

extern const digitalIO leds[6];

uint8_t cmdLed1On(cmdInstance *inst)
{
	Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, leds[0].gpioPort, leds[0].gpioPin);
	return 0;
}

uint8_t cmdLed2On(cmdInstance *inst)
{
	Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, leds[1].gpioPort, leds[1].gpioPin);
	return 0;
}

uint8_t cmdLed3On(cmdInstance *inst)
{
	Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, leds[2].gpioPort, leds[2].gpioPin);
	return 0;
}

uint8_t cmdLed4On(cmdInstance *inst)
{
	Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, leds[3].gpioPort, leds[3].gpioPin);
	Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, leds[4].gpioPort, leds[3].gpioPin);
	Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, leds[5].gpioPort, leds[3].gpioPin);
	return 0;
}

__attribute__ ((used,section(".cmdCommands")))
const commandEntry commandsPart01 [] = {
		{
				"led1On", /* Command name */
				cmdLed1On, /* Associated function */
				"led1On: Enciende el led 1 presente en la placa de desarrollo" /* Command help */
		},
		{
				"led2On", /* Command name */
				cmdLed2On, /* Associated function */
				"led2On: Enciende el led 2 presente en la placa de desarrollo" /* Command help */
		},
		{
				"led3On", /* Command name */
				cmdLed3On, /* Associated function */
				"led3On: Enciende el led 3 presente en la placa de desarrollo" /* Command help */
		},
		{
				"led4On", /* Command name */
				cmdLed4On, /* Associated function */
				"led4On: Enciende el led 4 presente en la placa de desarrollo" /* Command help */
		}

};
